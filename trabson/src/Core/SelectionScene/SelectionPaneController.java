package Core.SelectionScene;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import PersonClasses.Worker;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SelectionPaneController implements Initializable {
    private Boolean isAdmin;
    private Worker currentWorker;
    @FXML Pane workerTable;


    public void SetControllerWorker(Worker worker, boolean adm){
        currentWorker = worker;
        isAdmin = adm;
        if(adm)
            adminOption();

    }
    public void SetControllerAdmin(boolean adm){
        isAdmin = adm;
        adminOption();
    }

    void adminOption(){
        workerTable.setVisible(true);
    }


    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }


    public void CreateWorkerButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/CreateWorkerScene/CreateWorkerPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();
    }

    public void edirWorkerButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/EditWorkerScene/EditWorkerPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();

    }

    public void removeWorkerButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/RemoveWorkerScene/RemoveWorkerPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();

    }

    public void createClientButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/CreateClientScene/CreateClientPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();

    }

    public void editClientButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/EditClientScene/EditClientPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();

    }

    public void removeClientButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/RemoveClientScene/RemoveClientPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();

    }

    public void registerStudentButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/CreateWorkerScene/CreateWorkerPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();

    }

    public void editStudentButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/CreateWorkerScene/CreateWorkerPane.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();
    }

    public void removeStudentButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("SISAC");
        stage.show();

    }
}
