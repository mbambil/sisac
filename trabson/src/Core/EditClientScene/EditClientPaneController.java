package Core.EditClientScene;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import PersonClasses.Client;
import utils.ClientList;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EditClientPaneController implements Initializable {
    @FXML
    CheckBox checkStudent;
    @FXML
    Pane editableOptionsPane;

    @FXML
    Label searchError, updateOK;
    @FXML
    TextField newName, newAddress, newCity, newState, newPhone, newAge, clientName;
    private Client currentClient;
    private ClientList clientList;
    private int index;


    private int editData(Client client) {
        int update = 0;
        if (!newName.getText().equals("")) {
            client.updateName(newName.getText());
            update++;
        }
        if (!newAddress.getText().equals("")) {
            client.updateAdress(newAddress.getText());
            update++;
        }
        if (!newCity.getText().equals("")) {
            client.updateCity(newCity.getText());
            update++;
        }
        if (!newState.getText().equals("")) {
            client.updateState(newState.getText());
            update++;
        }
        if (!newAge.getText().equals("")) {
            client.updateAge(newAge.getText());
            update++;
        }
        if (!newPhone.getText().equals("")) {
            client.updatePhone(newPhone.getText());
            update++;
        }
        if (checkStudent.isSelected())
            client.updateIsStudent(true);
        else
            client.updateIsStudent(false);

        return update;

    }
    public void searchClientHandlerButton(ActionEvent actionEvent) throws IOException {
        clientList =new ClientList();
        if((index = clientList.search(clientName.getText(), clientList.getClientList())) >= 0 ){
            currentClient = clientList.getClient(index);
            searchError.setVisible(false);
            editableOptionsPane.setVisible(true);

        }
        else{
            updateOK.setVisible(false);
            searchError.setVisible(true);
            editableOptionsPane.setVisible(false);
        }
    }

    public void ConfirmButtonHandler(ActionEvent actionEvent) throws IOException {

        if(editData(currentClient)> 0){

            updateOK.setVisible(true);
            newName.clear();
            newAddress.clear();
            newCity.clear();
            newState.clear();
            newPhone.clear();
            newAge.clear();
            checkStudent.setSelected(false);
            editableOptionsPane.setVisible(false);
            clientList.replace(index, currentClient);
        }

    }
    @FXML
    private javafx.scene.control.Button exit;

    @FXML
    public void HandleButtonExit(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) exit.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
