package Core.CreateClientScene;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.ClientList;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CreateClientPaneController implements Initializable {
    @FXML
    CheckBox checkStudent;
    @FXML
    TextField newName, newAddress, newCity, newState, newPhone, newAge;
    @FXML
    Label nameError, addressError, cityError, stateError, phoneError, ageError, registerOK, registerError;

    private void createClient() throws IOException {
        int index;

        ClientList clientList = new ClientList();

        index = clientList.search(newName.getText(), clientList.getClientList());
        if(index == -1){
            clientList.add(newName.getText(), newAddress.getText(), newCity.getText(), newState.getText(),
                    newPhone.getText(), Integer.parseInt(newAge.getText()), checkStudent.isSelected());
            registerOK.setVisible(true);
            registerError.setVisible(false);
            nameError.setVisible(false);
            addressError.setVisible(false);
            cityError.setVisible(false);
            stateError.setVisible(false);
            phoneError.setVisible(false);







        }
        else{
            registerError.setVisible(true);
            registerOK.setVisible(false);
        }
    }
    public void ConfirmButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        int error = 0;
        nameError.setVisible(false);
        addressError.setVisible(false);
        cityError.setVisible(false);
        stateError.setVisible(false);
        ageError.setVisible(false);
        phoneError.setVisible(false);

        if(newName.getText().equals("")){
            nameError.setVisible(true);
            error = 1;
        }
        if(newAddress.getText().equals("")){
            addressError.setVisible(true);
            error = 1;
        }
        if(newCity.getText().equals("")){
            cityError.setVisible(true);
            error = 1;
        }
        if(newState.getText().equals("")){
            stateError.setVisible(true);
            error = 1;
        }
        if(newAge.getText().equals("")){
            ageError.setVisible(true);
            error = 1;
        }
        if(newPhone.getText().equals("")){
            phoneError.setVisible(true);
            error = 1;
        }

        if(error != 1){
            createClient();
        }
    }

    @FXML
    private javafx.scene.control.Button exit;

    @FXML
    public void HandleButtonExit(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) exit.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
