package Core.RemoveClientScene;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.ClientList;

import java.io.IOException;

public class RemoveClientPaneController {
    @FXML
    TextField clientName;
    @FXML
    Label removeError, removeOK;

    public void HandleConfirmButton(ActionEvent actionEvent) throws IOException {
        ClientList clientList = new ClientList();
        int index;
        if((index = clientList.search(clientName.getText(), clientList.getClientList())) >= 0){
            clientList.remove(clientList.getClient(index));
            removeError.setVisible(false);
            removeOK.setVisible(true);

        }
        else{
            removeError.setVisible(true);
            removeOK.setVisible(false);
        }

    }
    @FXML
    private javafx.scene.control.Button exit;

    @FXML
    public void HandleButtonExit(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) exit.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
}
