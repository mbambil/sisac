package Core.LoginScene;

//import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Core.SelectionScene.SelectionPaneController;
import PersonClasses.Worker;


        import utils.WorkerList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;


import javafx.event.ActionEvent;
/**
 * FXML Controller class
 *
 * @author kxaba
 */
public class Controller implements Initializable {
    @FXML
    TextField UserLogin;
    @FXML
    PasswordField UserPsswd;
    @FXML
    Label LoginError;
    Worker currentWorker;

    private boolean validateLogin(String login, String password) throws IOException {
        int index;
        WorkerList workerList = new WorkerList();
        if ((index = workerList.validateLogin(login)) >= 0) {
            if(workerList.validatePassword(index, password)){
                currentWorker = workerList.getWorker(index);
                return true;
            }
        }
        return false;

    }

    @FXML
    private void handleLoginButton(ActionEvent event) throws IOException {
        String Ulogin = UserLogin.getText();
        String Upsswd = UserPsswd.getText();
        boolean adm = false;
        if (validateLogin(Ulogin, Upsswd) || (adm = (Ulogin.equals("admin") && Upsswd.equals("admin")))) {

            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Core/SelectionScene/SelectionPane.fxml"));
            Parent root = loader.load();
            SelectionPaneController menu = loader.getController();
            if(adm)
                menu.SetControllerAdmin(adm);
            else
                menu.SetControllerWorker(currentWorker, adm);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("SISAC");
            stage.show();

        } else {
            LoginError.setVisible(true);
        }
    }

    @FXML
    private javafx.scene.control.Button exit;

    @FXML
    private void HandleButtonExit(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) exit.getScene().getWindow();
        // do what you have to do
        stage.close();
    }


    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
