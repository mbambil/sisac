package Core.CreateWorkerScene;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import utils.WorkerList;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CreateWorkerPaneController implements Initializable {
    @FXML
    TextField newName, newAddress, newCity, newState, newPhone, newCPF, newPsswd, psswdConfirm;
    @FXML
    Label nameError, addressError, cityError, stateError, phoneError, cpfError, passwordError, registerOK, registerError;
    WorkerList workerList;

    private void CreateWorker() throws IOException {
        int index;

        workerList = new WorkerList();

        index = workerList.search(newCPF.getText());
        if(index == -1){
            workerList.add(newName.getText(), newAddress.getText(), newCity.getText(), newState.getText(),
                    newPhone.getText(), newCPF.getText(), newPsswd.getText());
            registerOK.setVisible(true);
            registerError.setVisible(false);
            nameError.setVisible(false);
            addressError.setVisible(false);
            cityError.setVisible(false);
            stateError.setVisible(false);
            cpfError.setVisible(false);
            phoneError.setVisible(false);
            passwordError.setVisible(false);







        }
        else{
            registerError.setVisible(true);
            registerOK.setVisible(false);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void ConfirmButtonHandler(javafx.event.ActionEvent actionEvent) throws IOException {
        int error = 0;
        if(newName.getText().equals("")){
            nameError.setVisible(true);
            error = 1;
        }
        if(newAddress.getText().equals("")){
            addressError.setVisible(true);
            error = 1;
        }
        if(newCity.getText().equals("")){
            cityError.setVisible(true);
            error = 1;
        }
        if(newState.getText().equals("")){
            stateError.setVisible(true);
            error = 1;
        }
        if(newCPF.getText().equals("")){
            cpfError.setVisible(true);
            error = 1;
        }
        if(newPhone.getText().equals("")){
            phoneError.setVisible(true);
            error = 1;
        }
        if(newPsswd.getText().equals("") || psswdConfirm.getText().equals("")){
            passwordError.setVisible(true);
            error = 1;
        }

        if(error != 1){
            CreateWorker();
        }
    }

    @FXML
    private javafx.scene.control.Button exit;

    @FXML
    public void HandleButtonExit(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) exit.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

}
