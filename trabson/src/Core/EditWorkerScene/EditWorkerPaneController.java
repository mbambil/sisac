package Core.EditWorkerScene;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import PersonClasses.Worker;
import utils.WorkerList;

import java.io.IOException;

public class EditWorkerPaneController {
    @FXML
    public Pane editableOptionsPane;
    @FXML
    public Label passwordError, searchError, updateOK;
    @FXML
    TextField newName, newAddress, newCity, newState, newPhone, newCPF, newPsswd, psswdConfirm, currentCPF;
    private Worker currentWorker;
    private WorkerList workerList;
    private int index;

    private int editData(Worker worker){
        int update = 0;
        if(!newPsswd.getText().equals("") && !psswdConfirm.getText().equals("")){
            if(newPsswd.getText().equals("") || psswdConfirm.getText().equals("")){
                passwordError.setVisible(true);
                return 0;
            }
            else {
                worker.updatePassword(newPsswd.getText());
                update++;
            }
        }
        if(!newName.getText().equals("")){
            worker.updateName(newName.getText());
            update++;
        }
        if(!newAddress.getText().equals("")){
            worker.updateAdress(newAddress.getText());
            update++;
       }
        if(!newCity.getText().equals("")){
            worker.updateCity(newCity.getText());
            update++;
        }
        if(!newState.getText().equals("")){
            worker.updateState(newState.getText());
            update++;
        }
        if(!newCPF.getText().equals("")){
            worker.updateIdentifier(newCPF.getText());
            update++;
        }
        if(!newPhone.getText().equals("")){
            worker.updatePhone(newPhone.getText());
            update++;
        }

        return update;
    }

    public void SearchCPFButtonHandler(ActionEvent actionEvent) throws IOException {
        workerList =new WorkerList();
        if((index = workerList.search(currentCPF.getText())) >= 0 ){
            currentWorker = workerList.getWorker(index);
            searchError.setVisible(false);
            editableOptionsPane.setVisible(true);

        }
        else{
            updateOK.setVisible(false);
            searchError.setVisible(true);
            editableOptionsPane.setVisible(false);
        }
    }

    public void ConfirmButtonHandler(ActionEvent actionEvent) throws IOException {

        if(editData(currentWorker)> 0){

            updateOK.setVisible(true);
            passwordError.setVisible(false);
            newName.clear();
            newAddress.clear();
            newCity.clear();
            newState.clear();
            newPhone.clear();
            newCPF.clear();
            newPsswd.clear();
            psswdConfirm.clear();
            currentCPF.clear();
            editableOptionsPane.setVisible(false);
            workerList.replace(index, currentWorker);
        }

    }
    @FXML
    private javafx.scene.control.Button exit;

    @FXML
    public void HandleButtonExit(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) exit.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
}
