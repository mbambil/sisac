package Core.EditStudentScene;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import PersonClasses.Client;
import utils.ClientList;

import java.io.IOException;
import java.util.LinkedList;

public class EditStudentPaneController {
    @FXML
    Label updateOK, searchError;
    @FXML
    Button confirm;
    @FXML
    TextField studentName;
    @FXML
    Pane editOptionsPane;
    @FXML
    ComboBox obiBox, testDateBox;
    private Client currentStudent;

    public void SearchNameButtonHandler(ActionEvent actionEvent) throws IOException {
        ClientList clientList = new ClientList();
        LinkedList<Client> studentList = clientList.getStudentList();
        int index;
        if((index = clientList.search(studentName.getText(), studentList)) >= 0 ){
            currentStudent = clientList.getClient(index);
            searchError.setVisible(false);
            editOptionsPane.setVisible(true);

        }
        else{
            updateOK.setVisible(false);
            searchError.setVisible(true);
            editOptionsPane.setVisible(false);
        }
    }
}
