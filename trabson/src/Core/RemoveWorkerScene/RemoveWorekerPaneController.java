package Core.RemoveWorkerScene;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.WorkerList;

import java.io.IOException;

public class RemoveWorekerPaneController {
    @FXML
    TextField currentCPF;
    @FXML
    Label removeError, removeOK;


    public void HandleConfirmButton(ActionEvent actionEvent) throws IOException {
        WorkerList workerList = new WorkerList();
        int index;
        if((index = workerList.search(currentCPF.getText())) >= 0){
            workerList.remove(workerList.getWorker(index));
            removeError.setVisible(false);
            removeOK.setVisible(true);

        }
        else{
            removeError.setVisible(true);
            removeOK.setVisible(false);
        }

    }
    @FXML
    private javafx.scene.control.Button exit;

    @FXML
    public void HandleButtonExit(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) exit.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
}
