package PersonClasses;

import com.sun.istack.internal.Nullable;

public class Client extends Person {

   private boolean isStudent;
   private boolean isInDebt = false;
   private Payment payment;
   private Student student;

    private int age;



    public Client(String newName, String newAddress, String newCity, String newState, String newPhone, int newAge,
                  boolean registered){

        setName(newName);
        setAddress(newAddress);
        setCity(newCity);
        setState(newState);
        setPhone(newPhone);
        age = newAge;
        isStudent = registered;
        if(registered) {
            if (newAge > 14) {
                student = new Student(true);
            } else {
                student = new Student(false);
            }
        }


    }

    public boolean isStudent() {
        return isStudent;
    }

    public void setStudent(boolean student) {
        isStudent = student;
    }

    public int getAge() {
        return this.age;
    }


    public void setAge(int age) {
        this.age = age;
    }

    StringBuilder read(){
        StringBuilder data = new StringBuilder();

        data.append(getName()).append("\n");
        data.append(getnAddress()).append("\n");
        data.append(getCity()).append("\n");
        data.append(getState()).append("\n");
        data.append(getPhone());

        return data;


    }
    void setStudent(boolean status, @Nullable int monthsPaid){
        this.isStudent = status;
        if(this.isStudent){

            payment.create(monthsPaid);

        }
        else{
            if(this.isInDebt){
                System.out.printf("Faca o pagamento de %d parcelas para que possa ser cancelado\n", (payment.getNextMonthToBePaid()-payment.getActualMonth()));
            }
            else{
                payment = null;
                System.out.println("Aluno desligado");
            }
        }
    }

    public void updateName(String name){
        setName(name);
    }
    public void updateAdress(String adress){
        setAddress(adress);
    }
    public void updateCity(String city){
        setCity(city);
    }
    public void updateState(String state){
        setState(state);
    }
    public void updatePhone(String phone){
        setPhone(phone);
    }
    public void updateAge(String age){setAge(Integer.parseInt(age));}
    public void updateIsStudent(boolean student){setStudent(student);}
    private void setInDebt(boolean status){
        if(isStudent){
            this.isInDebt = status;
        }
    }

    boolean getisStudent(){
        return this.isStudent;
    }

    boolean getIsIndebt(){
        if(isStudent) {
            if(payment.getNextMonthToBePaid()-payment.getActualMonth() < 0){
                setInDebt(!this.isInDebt);
                return this.isInDebt;
            }
            else{
                return this.isInDebt;
            }
        }
        return false;
    }

}
