package PersonClasses;


import java.time.LocalDate;
import java.util.LinkedList;


public class Worker extends Person{

    boolean isAdmin = false;
    private String identifier;
    private String password;
    private LinkedList<LocalDate> testsDate;




    public Worker(String newName, String newAddress, String newCity, String newState, String newPhone, String cpf, String psswd){

        setName(newName);
        setAddress(newAddress);
        setCity(newCity);
        setState(newState);
        setPhone(newPhone);
        setIdentifier(cpf);
        setPassword(psswd);

    }

    StringBuilder read(){
        StringBuilder data = new StringBuilder();

        data.append(getName()).append("\n");
        data.append(this.identifier).append("\n");
        data.append(getnAddress()).append("\n");
        data.append(getCity()).append("\n");
        data.append(getState()).append("\n");
        data.append(getPhone());

        return data;


    }
    void setTestsDate(LocalDate date){
        this.testsDate.add(date);
    }

    private void setIdentifier(String cpf){
        this.identifier = cpf;
    }

    private void setPassword(String psswd){
        this.password = psswd;
    }

    public String getPassword(){
        return this.password;
    }

    public String getIdentifier(){
        return this.identifier;
    }

    public void updateName(String name){
        setName(name);
    }
    public void updateAdress(String adress){
        setAddress(adress);
    }
    public void updateCity(String city){
        setCity(city);
    }
    public void updateState(String state){
        setState(state);
    }
    public void updatePhone(String phone){
        setPhone(phone);
    }
    public void updateIdentifier(String identifier){
        setIdentifier(identifier);
    }
    public void updatePassword(String psswd){
        setPassword(psswd);
    }


}

