package PersonClasses;
import java.time.LocalDate;
import java.util.LinkedList;

enum Obi {
    branca(0, "Branca"), branca2(1, "Branca e Aamarela"),
    amarela(2, "Amarela"), amarela2(3, "Amarela e laranja"),
    laranja(4, "Laranja"), laranja2(5, "Laranja e roxa"),
    roxa(6, "Roxa"), roxa2(7, "Roxa e verde"), verde(8, "Verde"),
    verde2(9, "verde e Azul"), azul(10, "Azul"),
    azul2(11, "Azul e Marrom"), marrom(12, "Marrom"),
    marrom2(13, "Marrom e Vermelha"), vermelha(14, "Vermelha"),
    vermelha2(15, "Vermelha e preta"), preta(16, "preta");

    private String description;
    private int value;

    Obi(int value, String desctiption) {
        this.description = desctiption;
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }

}
public class Student{

    private LocalDate nextTest;
    private LinkedList<Object> currentObi;
    private int numberOfTries;
    private boolean isAdult;



    public Student(boolean adult){

        this.nextTest = null;
        this.currentObi = null;
        this.numberOfTries = 0;
        this.isAdult = adult;

    }


    public LocalDate getNextTest() {
        return nextTest;
    }

    public int getNumberOfTries() {
        return numberOfTries;
    }

    public LinkedList<Object> getCurrentObi() {
        return currentObi;
    }

    public boolean isAdult() {
        return isAdult;
    }

    void setNextTest(LocalDate date){
        this.nextTest = date;
    }

    void setNumberOfTries(boolean result){
        if(result){
            this.numberOfTries = 0;
        }
        else{
            this.numberOfTries +=1;
            setNextTest(null);
        }
    }

    void setCurrentObi(int obi){

        if(currentObi.size() == 0){
            this.currentObi.addFirst(obi);
            this.currentObi.addLast(Obi.branca.getDescription());
        }
        else {
            if(isAdult)
                obi +=1;

            for (Obi x : Obi.values()) {
                if (x.getValue() == obi) {
                    if (x.getValue() > (int) this.currentObi.get(0)) {
                        this.currentObi.removeFirst();
                        this.currentObi.removeLast();
                        this.currentObi.addFirst(x.getValue());
                        this.currentObi.addLast(x.getDescription());
                        setNumberOfTries(true);
                        setNextTest(null);


                    }
                }
            }
        }
    }




}
