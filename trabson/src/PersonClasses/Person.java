package PersonClasses;

abstract class Person {

    private String name;
    private String address;
    private String city;
    private String state;
    private String phone;


    public String getName(){
        return this.name;
    }
    public String getnAddress(){
        return this.address;
    }

    public String getCity(){
        return this.city;
    }

    public String getState(){
        return this.state;
    }

    public String getPhone(){
        return this.phone;
    }
    void setName(String newName){
        this.name = newName;
    }
    void setAddress(String newAddress){
        this.address = newAddress;
    }
    void setCity(String newCity){
        this.city = newCity;
    }
    void setState(String newState){
        this.state = newState;
    }
    void setPhone(String newPhone){
        this.phone = newPhone;
    }

}
