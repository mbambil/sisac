package PersonClasses;


import java.util.Calendar;

public class Payment {

    private int monthsPaid;
    private int fidelityTime = 0;
    private int actualMonth;
    private int nextMonthToBePaid;
    String Receipt;

    void create(int newMonthsPaid){
        setActualMonth();
        setMonthsPaid(newMonthsPaid);
        setFidelityTime(newMonthsPaid);
        setNextMonthToBePaid(this.actualMonth, this.monthsPaid);


    }
    private void setMonthsPaid(int newMonthspaid){
        this.monthsPaid = newMonthspaid;
    }
    private void setFidelityTime(int newFidelityTime){
        this.fidelityTime += newFidelityTime;
    }

    private void setActualMonth(){
        Calendar date = Calendar.getInstance();

        this.actualMonth = date.get(Calendar.MONTH)+1;


    }

    private void setNextMonthToBePaid(int actual, int paid){
        this.nextMonthToBePaid = actual + paid;

    }

    int getMonthsPaid(){
        return this.monthsPaid;
    }

    int getFidelityTime(){
        return  this.fidelityTime;
    }
    int getActualMonth(){
        return  this.actualMonth;
    }
    int getNextMonthToBePaid(){
        return this.nextMonthToBePaid;
    }



}
