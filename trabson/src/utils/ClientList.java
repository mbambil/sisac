package utils;

import PersonClasses.Client;

import java.io.*;
import java.util.LinkedList;

public class ClientList {
    private LinkedList<Client> clientList;
    private LinkedList<Client> studentList;

    public  ClientList() throws IOException {
        clientList = new LinkedList<Client>();
        studentList = new LinkedList<Client>();
        String line;

        try(BufferedReader br = new BufferedReader(new FileReader("clients.csv"))){

            while((line = br.readLine()) != null ){
                String[] client = line.split(",");
                clientList.addLast(new Client(client[0],client[1], client[2], client[3], client[4],
                        Integer.parseInt(client[5]), Boolean.parseBoolean(client[6])));
                if(Boolean.parseBoolean(client[6])){
                    studentList.addLast(new Client(client[0],client[1], client[2], client[3], client[4],
                            Integer.parseInt(client[5]), Boolean.parseBoolean(client[6])));
                }
            }
        }

    }

    public void add(String newName, String newAddress, String newCity, String newState, String newPhone, int newAage, boolean student)
            throws IOException {
        this.clientList.addLast(new Client(newName,newAddress, newCity, newState, newPhone, newAage, student));
        verifyStudent(clientList.getLast());
        writeCSV("clients.csv", clientList);
    }

    public boolean remove(Client client) throws IOException {
        if(clientList.contains(client)){
            this.clientList.remove(client);;
            if(client.isStudent()){
                this.studentList.remove(client);
            }
            writeCSV("clients.csv", clientList);
            writeCSV("studemts.csv", studentList);
            return true;
        }


        return false;
    }
    public void replace(int index, Client client) throws IOException {
        this.clientList.remove(index);
        this.clientList.addLast(client);
        writeCSV("clients.csv", clientList);


    }

    public int search(String name, LinkedList<Client> list){
        for (Client client: list) {
            if(client.getName().equals(name)){
                return clientList.indexOf(client);
            }
        }
        return -1;
    }

    public Client getClient(int index){
        return this.clientList.get(index);
    }

    private void verifyStudent(Client client) throws IOException {
        if(client.isStudent()){
            studentList.addLast(client);
            writeCSV("students.csv", studentList);
        }

    }

    public LinkedList<Client> getStudentList() {
        return studentList;
    }

    public LinkedList<Client> getClientList() {
        return clientList;
    }

    private void writeCSV(String file, LinkedList<Client> list) throws IOException {
        FileWriter fw = new FileWriter(new File(file));
        for (Client client: list) {
            String str = client.getName() + "," + client.getnAddress() + "," + client.getCity() + "," + client.getState() + ","
                    + client.getPhone() + "," + client.getAge() + "," + client.isStudent() + "\n";
            fw.append(str);
        }
        fw.close();
    }
}
