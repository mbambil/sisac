package utils;
import PersonClasses.Worker;
import java.io.*;
import java.util.LinkedList;

public class WorkerList {
    private LinkedList<Worker> workerList;

    public WorkerList() throws IOException {
        workerList = new LinkedList<Worker>();
        String line;

        try(BufferedReader br = new BufferedReader(new FileReader("workers.csv"))){

            while((line = br.readLine()) != null ){
                String[] worker = line.split(",");
                if(!worker[0].equals("admin")){
                    workerList.addLast(new Worker(worker[0],worker[1], worker[2], worker[3], worker[4], worker[5], worker[6]));
                }
            }
        }

    }

    public void add(String newName, String newAddress, String newCity, String newState, String newPhone, String newCPF,
                    String newPassword) throws IOException {
        Worker newWorker = new Worker(newName,newAddress, newCity, newState, newPhone, newCPF, newPassword);
        this.workerList.addLast(newWorker);
        writeCSV();
    }

    public boolean remove(Worker worker) throws IOException {
        if(workerList.contains(worker)){
            this.workerList.remove(worker);
            writeCSV();
            return true;
        }

        return false;
   }

    public void replace(int index, Worker worker) throws IOException {
        this.workerList.remove(index);
        this.workerList.addLast(worker);
        writeCSV();


    }

    public int search(String cpf){
        for (Worker worker: workerList) {
            if(worker.getIdentifier().equals(cpf)){
                return workerList.indexOf(worker);
            }
        }
        return -1;
    }
    public int validateLogin(String login){
        for (Worker worker: workerList) {
            if(worker.getIdentifier().equals(login)){
                return workerList.indexOf(worker);
            }
        }
        return -1;
    }
    public boolean validatePassword(int index, String password){
        return workerList.get(index).getPassword().equals(password);
    }

    public Worker getWorker(int index){
        return this.workerList.get(index);
    }

    private void writeCSV() throws IOException {
        FileWriter fw = new FileWriter(new File("workers.csv"));
        for (Worker w1: workerList) {
            String str = w1.getName() + "," + w1.getnAddress() + "," + w1.getCity() + "," + w1.getState() + ","
                    + w1.getPhone() + "," + w1.getIdentifier() + "," + w1.getPassword() + "\n";
            fw.append(str);
        }
        fw.close();
    }
}

